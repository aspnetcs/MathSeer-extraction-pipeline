'''
 simplified version of the well-known easydict package
'''

class EasyDict(object):
    def __init__(self, opt): self.opt = opt

    def __getattribute__(self, name):
        if name == 'opt' or name.startswith("_") or name not in self.opt:
            return object.__getattribute__(self, name)
        else: return self.opt[name]

    def __setattr__(self, name, value):
        if name == 'opt': object.__setattr__(self, name, value)
        else: self.opt[name] = value

    def __getitem__(self, name):
        return self.opt[name]
    
    def __setitem__(self, name, value):
        self.opt[name] = value

    def __contains__(self, item):
        return item in self.opt

    def __repr__(self):
        return self.opt.__repr__()

    def update(self, opt):
        self.opt.update(opt)

    def keys(self):
        return self.opt.keys()

    def values(self):
        return self.opt.values()

    def items(self):
        return self.opt.items()
