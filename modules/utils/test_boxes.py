import cv2
import glob
import os
import numpy as np
import csv
import matplotlib.pyplot as plt


def test_anns():
    anno_data = np.genfromtxt('ScanSSD/chem_data/US20030008869A1.csv', delimiter=',')[:,1:]
    img = cv2.imread('ScanSSD/chem_data/US20030008869A1/3.png', 1)
    anno_data = anno_data[anno_data[:,0] == 3][:,1:].astype(np.int)

    for ann in anno_data:
        img = cv2.rectangle(img, (ann[0], ann[1]), (ann[0]+ann[2], ann[1]+ann[3]), (0,0,255), 5)

    plt.imshow(img)
    plt.show()


def convert_anns(ann_file, output_dir_anns, output_list, orig_dpi=300, new_dpi=600):
    anns = []
    ann_list = []
    scale = int(new_dpi/orig_dpi)
    with open(ann_file, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            anns.append(row)
    anns = np.asarray(anns).reshape((-1,6))[1:,:]
    pdfs = anns[:, 0]
    pdfs = [pdf.replace('.tif', '') for pdf in pdfs]
    pdf_names = np.unique(pdfs)
    pdfs = np.asarray(pdfs)
    anns[:,0] = pdfs
    for pdf_name in pdf_names:
        output_path = os.path.join(output_dir_anns, pdf_name)
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        pdf_anns = anns[anns[:,0] == pdf_name]
        page_nums = np.unique(pdf_anns[:,1])
        for page in page_nums:
            ann_list.append([pdf_name, page])
            page_anns = pdf_anns[pdf_anns[:,1] == page].copy()
            page_anns = page_anns[:,2:].astype(np.int)
            page_anns[:,2] += page_anns[:,0]
            page_anns[:,3] += page_anns[:,1]
            page_anns *= scale
            out_page = open(output_path+'/'+page+'.pmath', 'a')
            np.savetxt(out_page, page_anns, fmt='%.1f', delimiter=',')
            out_page.close()

    # Write the pdf_list
    with open(output_list, 'w') as f:
        if len(ann_list):
            for ann in ann_list[:-1]:
                f.write(ann[0]+'/'+ann[1]+'\n')
            f.write(ann_list[-1][0]+'/'+ann_list[-1][1])


if __name__ == '__main__':
    convert_anns('/home/abhisek/Desktop/Chem/Benchmarks_Tasks/data'
                 '/CLEF-IP_2012/segmentation/CLEF2012-chem-segment-TEST-SOLUTIONS'
                 '/clipdata.csv',
                 'ScanSSD/chem_data/annotations', 'ScanSSD/chem_data_test')
