import yaml
from sscraper_service import create_and_save_sscraper_services
from scanssd_service import create_and_save_scanssd_services
from qdgga_service import create_and_save_qdgga_services
from nodeportloadbalancer import create_node_port_load_balancers
from namespace import create_namespace

if __name__ == '__main__':

    # clean infra dir

    #open
    with open('aio-config.yml', 'r') as file:
        config = yaml.safe_load(file)

    create_namespace(config['parameters']['namespace'], 'infra_v1')

    sscraper_services = create_and_save_sscraper_services(
        servers=config['servers']['sscraper'],
        namespace=config['parameters']['namespace'],
        spec_dir='infra_v1'
    )

    scanssd_services = create_and_save_scanssd_services(
        servers=config['servers']['scanssd'],
        namespace=config['parameters']['namespace'],
        spec_dir='infra_v1'
    )

    qdgga_services = create_and_save_qdgga_services(
        servers=config['servers']['qdgga'],
        namespace=config['parameters']['namespace'],
        spec_dir='infra_v1'
    )
