import os
from copy import copy
from io import BytesIO
from pydantic import BaseModel
from modules.aiopipeline.model.Combined import Combined


class PostScanSSD(BaseModel):
    file_name: str
    num_pages: int
    pdf: BytesIO
    xml: BytesIO
    csv: BytesIO

    def to_combined(self, tsv: BytesIO, region_tsv: BytesIO) -> Combined:

        return Combined(
            file_name=self.file_name,
            pdf=self.pdf,
            tsv=tsv,
            region_tsv=region_tsv,
            csv=self.csv,
            xml=self.xml,
            num_pages=self.num_pages
        )

    def save(self, output_dir):
        write_xml = open(
            os.path.join(output_dir, 'aio', 'sscraper', self.file_name.split('.')[0]+".xml"),
            'wb')
        write_xml.write(copy(self.xml).read())
        write_xml.close()

        write_csv = open(
            os.path.join(output_dir, 'aio', 'scanssd', self.file_name.split('.')[0]+".csv"),
            'wb')
        write_csv.write(copy(self.csv).read())
        write_csv.close()

    class Config:
        arbitrary_types_allowed = True
