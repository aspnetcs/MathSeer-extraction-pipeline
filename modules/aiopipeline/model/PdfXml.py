import logging
from io import BytesIO
import os
from pydantic import BaseModel
import aiofiles
from modules.aiopipeline.model.PostScanSSD import PostScanSSD


logger = logging.getLogger("PdfXml")


class PdfXml(BaseModel):
    file_name: str
    num_pages: int
    pdf: BytesIO
    xml: BytesIO

    def to_post_scannssd(self, csv) -> PostScanSSD:
        return PostScanSSD(
            file_name=self.file_name,
            pdf=self.pdf,
            xml=self.xml,
            csv=csv,
            num_pages=self.num_pages
        )

    def save(self, output_dir, collection):
        write_xml = open(
            os.path.join(output_dir, 'aio', collection, 'sscraper', self.file_name.split('.')[0]+".xml"),
            'wb')
        write_xml.write(self.xml.read())
        write_xml.close()

    class Config:
        arbitrary_types_allowed = True
