import asyncio
import logging
import os
from io import BytesIO
from queue import Empty
from typing import List, Dict
from multiprocessing import Queue, Event
import aiohttp
from copy import copy
from modules.aiopipeline.model.Combined import Combined
from modules.aiopipeline.model.PostParser import PostParser
from aiohttp import FormData
import zipfile
from modules.aiopipeline.workers.utils import all_true, check_status


logger = logging.getLogger('parsing')


def combined_to_post_parser(combined_queue: Queue,
                            post_parser_queue: Queue,
                            combined_done: List[Event],
                            parser_done: Event, 
                            config: Dict,
                            server_i: int):
    host = 'localhost'
    cur_server_conf = config['servers']['qdgga'][server_i]
    cur_server_name = list(cur_server_conf.keys())[0]
    parser_port = cur_server_conf[cur_server_name]['ports']
    dpi = config['parameters']['dpi']
    loop = asyncio.get_event_loop()
    while not (all_true(combined_done) and combined_queue.empty()):
        try:
            cur_combined: List[Combined] = combined_queue.get(timeout=1)
            while((not combined_queue.empty()) and (len(cur_combined) < 2)):
                try: 
                    n_combined = combined_queue.get(timeout=1)
                    cur_combined.extend(n_combined)
                except Empty:
                    pass
                except Exception as e:
                    logger.warning(e)
            post_parser_list = loop.run_until_complete(send_combined_to_parser(cur_combined, host, parser_port, dpi))
            post_parser_queue.put(post_parser_list)
        except Empty:
            pass
        except Exception as e:
            logger.warning(e)

    parser_done.set()


async def send_combined_to_parser(all_combined: List[Combined], host, parser_port, dpi) -> List[PostParser]:

    endpoint = f"http://{host}:{parser_port}/predict-pdf?dpi={dpi}"
    timeout = aiohttp.ClientTimeout(total=1200)
    async with aiohttp.ClientSession(timeout=timeout) as session:
        coroutines = []
        for c in all_combined:
            data = FormData()
            logger.info(f"adding {c.file_name} to parser batch request for processing")
            data.add_field('pdf_list',
                           copy(c.pdf),
                           filename=os.path.basename(c.file_name),
                           content_type='application/pdf')
            data.add_field('tsv_list',
                           copy(c.tsv),
                           filename=os.path.splitext(os.path.basename(c.file_name))[0] + '.tsv',
                           content_type='text/tab-separated-values')

            co_route = session.post(endpoint, data=data)
            coroutines.append(co_route)
        responses = await asyncio.gather(*coroutines)
        post_parser_list: List[PostParser] = []
        for r in responses:

            n_post_parser_list = await extract_response_into_post_parser_list(r, all_combined)
            post_parser_list += n_post_parser_list
        return post_parser_list


async def extract_response_into_post_parser_list(response, all_combined: List[Combined]) -> List[PostParser]:
    await check_status(response, logger)
    zip_bytes = await response.content.read()
    z = zipfile.ZipFile(BytesIO(zip_bytes))

    # a zip file of all the tsvs from the service is returned
    post_parser_list = []
    for file_name in z.namelist():
        logger.info(f"extracting {file_name} from parser service response and adding to post parser list")
        tsv_bytes = BytesIO(z.read(file_name))
        for c in all_combined:
            if os.path.splitext(c.file_name)[0] in file_name:
                post_parser_list.append(c.to_post_parser(tsv_bytes))

    return post_parser_list
