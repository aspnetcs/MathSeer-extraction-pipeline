from io import BytesIO
from typing import List
from multiprocessing import Queue, Event
import os
from modules.aiopipeline.model.LoadedPdf import LoadedPdf
import logging
from PyPDF2 import PdfFileReader
from copy import copy


logger = logging.getLogger('pdf_loader')


def pdf_loader(input_dir: str,
               pdfs: List[str],
               loaded_queue: Queue,
               mini_batch_size: int,
               loader_done: Event):

    page_limit = os.getenv("PAGE_LIM", 100)
    # keep putting pdfs on the queue while there are pdfs to process
    while len(pdfs) > 0:
        # grab 1000 pdfs at a time so the sorting is manageable as N
        # of pdfs rises, also so there is not as much in memory at a given time
        next_1000_pdfs = [pdfs.pop() for _ in range(1000) if len(pdfs) > 0]
        loaded_pdfs = []
        while len(next_1000_pdfs) > 0:
            cur_pdf = next_1000_pdfs.pop()
            cur_pdf = os.path.join(input_dir, 'pdf', cur_pdf.strip("\n")+".pdf")
            logger.info(f"loading pdf: {cur_pdf}")
            try:
                number_of_pages = os.path.getsize(cur_pdf)
                f = open(cur_pdf, 'rb')
                raw_bytes = f.read()
                f.close()

                pdf_bytes = BytesIO(raw_bytes)
                pdf_file = PdfFileReader(copy(pdf_bytes))

                if pdf_file.getNumPages() <= page_limit:

                    loaded_pdf = LoadedPdf(
                        file_name=os.path.basename(cur_pdf),
                        pdf=pdf_bytes,
                        num_pages=number_of_pages
                    )
                    logger.info(f"loaded {loaded_pdf.file_name}")
                    loaded_pdfs.append(loaded_pdf)
                else:
                    logger.warning(f"{cur_pdf} has a page size of {pdf_file.getNumPages()} "
                                   f"which is greater than the specified page limit of {page_limit}. "
                                   f"It will not be processed.")

            except Exception as e:
                logger.warning(e)

        def sort_fn(cur_l_pdf: LoadedPdf):
            return cur_l_pdf.num_pages

        loaded_pdfs = sorted(loaded_pdfs, key=sort_fn, reverse=True)
        mini_batch = []
        while len(loaded_pdfs) > 0:
            mini_batch.append(loaded_pdfs.pop())
            if len(mini_batch) == mini_batch_size:
                loaded_queue.put(mini_batch)
                logger.info(f"{[l_pdf.file_name for l_pdf in mini_batch]} placed in loaded pdf queue")
                mini_batch = []

        if len(mini_batch) > 0:
            logger.info(f"{[l_pdf.file_name for l_pdf in mini_batch]} placed in loaded pdf queue")
            loaded_queue.put(mini_batch)

    loader_done.set()
    logger.info("Loader finished")
