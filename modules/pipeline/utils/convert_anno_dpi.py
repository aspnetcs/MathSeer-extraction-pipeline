import os
import glob
import argparse
import numpy as np


def rescale_anno(src_dir, tgt_dir, src_dpi, tgt_dpi):
    anno_paths = glob.glob(os.path.join(src_dir,'*'))
    for pdf in anno_paths:
        pdf_name = pdf.split('/')[-1]
        target_pdf_dir = os.path.join(tgt_dir, pdf_name)
        if not os.path.exists(target_pdf_dir):
            os.makedirs(target_pdf_dir)
        ann_files = glob.glob(os.path.join(pdf, '*.pmath'))
        for ann_file in ann_files:
            page_num = ann_file.split('/')[-1]
            page_anno = np.genfromtxt(ann_file, delimiter=',').reshape((-1,4))
            scaling_factor = tgt_dpi/src_dpi
            scaled_anno = (page_anno * scaling_factor).astype('int32')
            math_file = open(os.path.join(target_pdf_dir, page_num), 'w')
            np.savetxt(math_file, scaled_anno, fmt='%.1f', delimiter=',')
            math_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Convert pdf annotations")
    parser.add_argument(
        "--src_dir",
        default="src/ScanSSD/gtdb_data/annotations",
        type=str,
        help="Original Annotation Directory",
    )
    parser.add_argument(
        "--tgt_dir",
        default="src/ScanSSD/gtdb_data_150dpi/annotations",
        type=str,
        help="Target directory to save annotations",
    )
    parser.add_argument(
        "--src_dpi",
        default=600,
        type=int,
        help="Source dpi of the bounding box annotations",
    )
    parser.add_argument(
        "--tgt_dpi",
        default=150,
        type=int,
        help="Target dpi of the bounding box annotations",
    )

    args = parser.parse_args()

    rescale_anno(args.src_dir, args.tgt_dir, args.src_dpi, args.tgt_dpi)
