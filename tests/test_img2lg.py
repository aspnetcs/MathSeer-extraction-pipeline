import os
import os.path as osp
from glob import glob

import cv2
import pytest

from msp_settings import TEST_INPUTS_DIR
from utils.img2LG import extract_ccs

fimg_files = glob(osp.join(TEST_INPUTS_DIR, "img2lg", "*.PNG"))
counts = [5] * len(fimg_files)
testdata = zip(fimg_files, counts)


@pytest.mark.parametrize("file, count", testdata)
def test_extract_ccs(file, count):
    """Test number of extracted CCs"""
    img = cv2.imread(file)
    _, boxes = extract_ccs(img)
    assert count == len(boxes)
