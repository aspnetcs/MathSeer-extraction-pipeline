import os
import os.path as osp
from glob import glob

import cv2
import pytest

from msp_settings import LABELS_TRANS_CSV, MATHML_MAP_DIR, TEST_INPUTS_DIR
from utils.img2LG import extract_ccs
from utils.lgeval.src import lg2txt

lg_files = glob(osp.join(TEST_INPUTS_DIR, "lg2txt", "*.lg"))
target_mml_files = [lg_file[:-3] + "_target.mml" for lg_file in lg_files]
testdata = zip(lg_files, target_mml_files)


@pytest.mark.parametrize("lg_file, target_mml_file", testdata)
def test_lg2txt_punc(lg_file, target_mml_file):
    """Test PUNC relations mappings in lg to mml conversion"""
    mml_output = lg2txt.main(lg_file, MATHML_MAP_DIR, LABELS_TRANS_CSV)
    with open(target_mml_file, "r") as f:
        mml_target = f.read().strip()
    assert mml_output == mml_target
