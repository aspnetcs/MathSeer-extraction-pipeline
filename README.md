# MathSeer Extraction Pipeline

**Authors:** Ayush Kumar Shah, Abhisek Dey, Matt Langsenkamp, Richard Zanibbi   
([Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html), Rochester Institute of Technology, USA)

## Version 0.1.1 (September 2021)

**Purpose:** This pipeline extracts mathematical formulas from PDF documents.
It was developed at RIT (USA) for the
[MathSeer](https://www.cs.rit.edu/~dprl/mathseer) project. The pipeline processes a set of PDF documents, producing as output:

1. PDF character and symbol information in XML, from the [SymbolScraper](src/SymbolScraper) extension of [Apache PDFBox](https://pdfbox.apache.org/) (Java)
2. Bounding boxes for formulas in document page images, from our Scanning Single-Shot Detector model, [ScanSSD](src/ScanSSD) (PyTorch)
3. Presentation MathML representations for individual formulas, from the Query-Driven Global Graph Attention parser [QD-GGA](src/QD-GGA) (PyTorch)

In the current pipeline, outputs from the separate applications are combined using command-line scripts. We hope to provide a Docker-based version in the future.


**System Description and Citation.** A description of the system, which you should cite if you use our pipeline in your own work is available:

* Ayush Kumar Shah, Abhisek Dey, and Richard Zanibbi (2021). [A Math Formula Extraction and Evaluation Framework for PDF Documents](https://www.cs.rit.edu/~rlaz/files/ICDAR2021_MathSeer_Pipeline.pdf), Proc. International Conference on Document Analysis and Recognition (ICDAR).



## Installation

**Requirements**

1. Python
2. Conda (e.g., from Anaconda) and pip for Python environments
3. Java 
4. Maven (build tool for Java)
5. Unix shell (e.g., bash or zsh)
6. wget unix utility
7. GraphViz command line program, needed for visualization using `dot` for parse graphs -- available through linux package managers (e.g., `apt`) or [here](https://graphviz.org/))
8. libenchant-dev: needed for dictionary-based word filtering, available through linux package managers (e.g., `apt`) -- Ubuntu 1.6.0-11.3build1 amd64 used in our work to date. 

The pipeline has been tested primarily on Linux systems (specifically, Ubuntu 20.04 LTS).

Once you have these requirements installed, from the top directory of the project, you run the installation script by issuing:

```zsh
$ make
```
This will install all modules, and create a bash script named `run-msp` which can be used to run the pipeline (see instructions below).

To force the installation to continue after encountering errors, issue:

```zsh
$ make force
```

### Removal and Reinstallation
To remove all installed data and tools, issue:

```zsh
$ make clean
```
This is particularly useful if you want to rebuild the system from scratch ('cleanly').

To rebuild the entire system, including the conda environment, issue:

```zsh
$ make rebuild
```

### run-msp (main program)

After the program has been installed, the script `run-msp` is created. You can run this without arguments to see the command line
arguments and options. Examples and details are provided below.

### Installing Individual System Modules 

If you experience problems using the installation script, or would prefer to install components needed separately, here are some more detailed instructions for installation. 

<details>
<summary>
1. [SymbolScraper](src/SymbolScraper)
</summary>

**Dependencies:** Make sure that both Maven (3.6.0 for us - other versions may 
work fine) and Java 1.8 are installed on your system. Perl is used to 
generate bash scripts for running the program more easily.

**Operating System:** While this code should work on Windows systems as well,
we have only tested on Linux and MacOS X so far.

From the root directory, issue the command:

```zsh
$ make -C src/SymbolScraper
```

This will run the Maven
build system, download dependencies, etc., compile source files and generate .jar
files in `./target`. Finally, a bash script `bin/sscraper` is generated, so 
that the program can be easily used in different directories.

The `pom.xml` file can be modified to change the Maven build parameters. 
</details>

<details>
<summary>
2. [ScanSSD](src/ScanSSD)  and [QD-GGA](src/QD-GGA)
</summary>

It is recommended that you install Python packages using Python3.6.9 within a conda environment.

Using conda, from the root directory, issue the commands:

```zsh
$ conda create -n mathseer python=3.6.9
$ conda activate mathseer
(mathseer) $ pip install -r requirements.txt
```

</details>

<details>
<summary>
3. Download Neural Network Weights
</summary>

The installation script automatically downloads the network
weights needed from the dprl@RIT, and then move the files to the appropriate directories.

**If you encounter problems**, the installation script (`install`) may be read and modified using any standard text editor. 

</details>

<details>
<summary>
4. Install LgEval 
</summary>

1. Install the Python-based LgEval library from the [LgEval repository](https://gitlab.com/dprl/lgeval) **in the `modules/`** subdirectory. The tool requires some environment variables to be defined in your shell (e.g., using modifications to your `bash` or `zsh` configuration file).

2. Make sure that the `graphviz` package containing the `dot` program is installed on your system. This can be installed from standard Linux package managers (e.g., `apt`), and is also available for windows and macos.

3. Check that you can successfully call the `lg2dot` script from your shell after the installation. This is used to convert recognized formula structured in labeled graphs ('lg' files) to PDF for visualization of recognized structure and errors.

**Please Note:** the installation script sets up the environment for LgEval within `run-msp` itself. This means that unless you modify your shell conifguration script, `lg2dot` will not be available from the command line.

</details>

## Getting Started: Running the Pipeline (Math and Chem)





**Please Note:** Input directories must have the organization shown in the description of arguments for the `input_dir` parameter below. 

A sample of 5 ACL ([Association for Computational Linguistics Anthology](https://aclanthology.org)) papers have been included in `inputs/ACL/` to test running the pipeline.
A sample of 1 [CLEF-IP 2012](http://www.ifs.tuwien.ac.at/~clef-ip/download/2012/index.shtml) document has been included in `inputs/CLEF_TEST` to test running the pipeline in Chemistry mode. 

**Example Execution.**  If you were able to create the `run-msp` program using the `install` script, to run the example ACL files or CLEF, you can issue:

*Math Mode*
```zsh
$ make example
```
*Chemistry Mode*
```zsh
$ make chem-test
```

In the `Makefile` you can see commands for other (smaller) examples as well.

**Using run-msp.** `make example` is equivalent to issuing the following:

*Math Mode*
```zsh
$ ./run-msp -i ./inputs/ACL -o ./outputs2/ACL -c  1 -v 1
```
while `make chem-test` is equivalent to:

*Chemistry Mode*
```zsh
$ ./run-msp --input_dir ./inputs/CLEF_TEST_2 --output_dir ./outputs2/CLEF_TEST_2 --mode 1 -c 1 -v 1
```

which passes input files that are converted to images (`-c`), with results written to the `outputs/ACL` directory.

**Outputs** are written to the `outputs` directory. An example output for one of the ACL PDF documents is shown in `MathSeer-extraction-pipeline/outputs/ACL0001.`

**Logs:** The system produces logs during exectution, which may be found in the following places.

- The main log file is generated in the `logs/` directory.
- The log file corresponding to ScanSSD is generated in the
    `src/ScanSSD/ssd/logs/` directory.

## Summary of Command-Line Arguments

* (Required) `input_dir:` input directory organized as shown below.  

```	
	<input_dir>/
		pdf/			input pdf files 
		images/ 		PNG images for PDF pages (Optional) 
		pdf_list		PDF file names to process (one per line) 

```

See `MathSeer-extraction-pipelin/inputs/ACL/images/` for an example of how PDF page images are organized.


* (Required) `output_dir:` directory for storing results from the pipeline, organized as shown below.

```
	<output_dir>/
		qdgga/			QD-GGA configuration, TSV input & output
		scanssd/		CSV formula regions from ScanSSD
		sscraper/		XML character data from Symbol Scraper
		view/ 			(optional, with -v (visualizations))
```



* `log_dir:` directory to use for log outputs.

* `convert_pdfs:` convert PDF pages to png images. Not necessary if images already provided within the input directory (see above).

* `visualize (0/1):` generate visualizations for detection/recognition results, including HTML and images for detections and parse inputs. This adds a `view` directory to the list of output directories above.

**Optional Recognition Module Parameters**

* `batch_size:` set the batch size for ScanSSD (formula detection). 

* `qdgga_model:` parser network to use

* `last_epoch:` epoch number to use to defined QD-GGA network model weights

## Contributors

The MathSeer pipeline was developed by the [Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html) at the Rochester Institute of Technology (USA), for the [MathSeer](https://www.cs.rit.edu/~dprl/mathseer) project.

### Pipeline Update and Release Contributors

* **QD-GGA and LgEval Update:** Ayush Kumar Shah
* **ScanSSD:** Abhisek Dey
* **SymbolScraper:** Matt Langenkamp
* **Pipeline and Documentation:** Auyush Kumar Shah, Matt Langsenkamp, and Richard Zanibbi


First pipeline release was made in August 2021.

### Additional Contributors 

The following people made contributions between 2016 and 2020 while they were members of the dprl lab.

* **QD-GGA:** Wei Zhong, Kenny Davila, Michael Condon, Mahshad Mahdavi, Alex Keller
* **ScanSSD:** Parag Mali, Alex Keller
* **SymbolScraper:** Ritvik Joshi, Jessica Diehl, Alex Keller
* **Pipeline:** Alex Keller

## Support

This material is based upon work supported by the National Science Foundation (USA) under Grant Nos. IIS-1016815, IIS-1717997, and 2019897 (MMLI), and the Alfred P. Sloan Foundation under Grant No. G-2017-9827.

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the Alfred P. Sloan Foundation.
